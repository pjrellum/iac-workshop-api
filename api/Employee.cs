namespace api;
using Azure;
using Azure.Data.Tables;

public class Employee : ITableEntity
{
    public string PartitionKey { get; set; }
    
    public string RowKey { get; set; }
    
    public string? Name { get; set; }

    public string? Age { get; set; }

    public string? City { get; set; }

    public DateTimeOffset? Timestamp { get; set; }
    
    public ETag ETag { get; set; }
}

using Microsoft.AspNetCore.Mvc;

namespace api.Controllers;

[ApiController]
[Route("[controller]/[action]")]
public class HelloWorldController : ControllerBase
{
    private readonly string _name;

    private readonly ILogger<HelloWorldController> _logger;

    public HelloWorldController(ILogger<HelloWorldController> logger)
    {
        _logger = logger;
        try
        {
            var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "config", "my-name-is.txt"));
            _name = reader?.ReadLine() ?? "Empty";
        }
        catch
        {
            _name = "Error";
        }
    }

    [HttpGet(Name = "GetHelloWorld"), ActionName("Hi")]
    public string Hi()
    {
        return $"Hello, my name is {_name}!";
    }
}

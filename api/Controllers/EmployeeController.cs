using Microsoft.AspNetCore.Mvc;
using Azure.Data.Tables;

namespace api.Controllers;

[ApiController]
[Route("[controller]/[action]")]
public class EmployeesController : ControllerBase
{
    private readonly ILogger<EmployeesController> _logger;
    private readonly IConfiguration _configuration;

    public EmployeesController(IConfiguration configuration, ILogger<EmployeesController> logger)
    {
        _logger = logger;
        _configuration = configuration;
    }

    [HttpGet(Name = "GetEmployees"), ActionName("GetAll")]
    public async IAsyncEnumerable<Employee> GetAll()
    {
        TableClient tableClient;

        try
        {
            var serviceClient = new TableServiceClient(_configuration["StorageConnectionString"]);
            tableClient = serviceClient.GetTableClient("iacworkshopdatabase");
            await tableClient.CreateIfNotExistsAsync();
        }
        catch (Exception ex)
        {
            throw new Exception($"Error, exception: {ex.Message}");
        }

        await foreach (var item in tableClient.QueryAsync<Employee>())
        {
            yield return item;
        }
    }
}

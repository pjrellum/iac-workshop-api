FROM mcr.microsoft.com/dotnet/aspnet:6.0

WORKDIR api

ADD ./api/bin/Debug/net6.0 .

EXPOSE 80

ENV ASPNETCORE_URLS http://0.0.0.0:80

ENTRYPOINT ["dotnet", "api.dll"]

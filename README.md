# Create a Docker Image in Azure Container Registry containing an API

## Steps

1. Send the Dockerfile and the API to ACR for a Docker build using `az acr build --image iacworkshop/api:v1 --registry <nameofyourazurecontainerregistry>.azurecr.io --file Dockerfile .`
1. Watch and see Azure Container Registry do re remote build of your Docker image!
1. Locate and examine the image in your Container Registry.
